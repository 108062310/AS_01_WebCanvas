# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Check and Resize canvas when uploading image     | 1~5%     | Y         |


---

### How to use 
![](https://i.imgur.com/f7q6LrZ.png)

 Color Select
 Pen Width
 Text font / size

| Row   | Column 1  | Column 2  | Column 3  | Column 4   |
| -------- | --------- | --------- | --------- | ---------- |
| Row1        | Pen   | Eraser   | Text    | Refresh    |
| Row2       | Line   | Circle   | Triangle | Rectangle    |
| Row3       | Undo     | Redo     | Upload  | Download    |
| Row4 | Resize(op)        | -        | -         | -    |
 


    

### Additional Function description

- Check and Resize canvas when uploading image
    When uploading an image, if image size is bigger than canvas size (800 x 600), then reject upload image and alert it.
    ![](https://i.imgur.com/h2GCnId.png)
    If image size is smaller than canvas size, alert a confirm to let user choose resize canvas size to image size or not.
    ![](https://i.imgur.com/UkE9YqA.png)
    If confirm resize, than the "resize" button shows, and when clicked, canvas resize to default size (800 x 600) and refresh the canvas.
    ![](https://i.imgur.com/aaxC62b.png)


### Gitlab page link

https://108062310.gitlab.io/AS_01_WebCanvas


<style>
table th{
    width: 100%;
}
</style>