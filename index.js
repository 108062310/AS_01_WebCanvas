//canvas
var canvas = document.getElementById("canvas");
var hcanvas = document.getElementById("canvas2");
canvas.width = hcanvas.width =  800;
canvas.height = hcanvas.height = 600;
canvas.style.left = hcanvas.style.left = "120px";
canvas.style.top = hcanvas.style.top = "120px";
canvas.style.position = hcanvas.style.position = "absolute";
var ctx = canvas.getContext("2d");
var hctx = hcanvas.getContext("2d");

//pen properties
var pen = {
    width: 1,
    select: 'pen',
    inside: false
};

//text properties
var text = {
    texting: false,
    size: "28",
    font: '微軟正黑體'
};


//textbox
var textBox = document.getElementById('textBox');

// last known position
var pos = { x: 0, y: 0 };
var texting = false;

//history array
var historys = {
    step: -1,
    step_list: [],
    Push: function(){
        this.step++;
        this.step_list.push(canvas.toDataURL());
    },
    undo: function(){
        if(this.step > 0){
            this.step--;
            var img = new Image();
            img.src = this.step_list[this.step];
            img.onload = function() {
                canvas.width = img.width;
                canvas.height = img.height;
                ctx.drawImage(img,0,0);
            }
        }
        else{
            this.step--;
            ctx.clearRect(0, 0, canvas.width, canvas.height);
        }
    },
    redo: function(){
        if(this.step < this.step_list.length-1){
            this.step++;
            var img = new Image();
            img.src = this.step_list[this.step];
            img.onload = function() {
                canvas.width = img.width;
                canvas.height = img.height;
                ctx.drawImage(img,0,0);
            }
        }
    }
};

// add event listeners to trigger on different mouse events
document.addEventListener("mousemove", draw);
document.addEventListener("mousedown", setPosition);
document.addEventListener("mouseenter", setPosition);
document.addEventListener("mouseup", save_hisState);

//range bar value changed
function setDrawSize(){
    var textSize = document.getElementById('drawsize');
    var size = document.getElementById('sizebar');
    textSize.textContent = parseInt(size.value);
    pen.width = parseInt(size.value);
}

//click on different button
function setPenSelect(selected){
    pen.select = selected;
    canvas.style.cursor = hcanvas.style.cursor = "url(pic/" + pen.select + ".png), auto";
}

//change color
function changeColor(){
    var color = document.getElementById('color');
    ctx.strokeStyle = color.value;
    ctx.fillStyle = color.value;
}

//change font size/family
function changefont(){
    var size = document.getElementById('textSize');
    var font = document.getElementById('textfont');
    textBox.style.font = size.value + 'px ' + font.value;
    text.font = font.value;
    text.size = size.value;
    ctx.font = size.value + 'px ' + font.value;
}

// new position from mouse events
function setPosition(e) {
  pos.x = e.clientX;
  pos.y = e.clientY;
  //mouse in canvas
  if(e.clientX>=120 && e.clientX<=120+canvas.width && e.clientY>=120 && e.clientY<=120+canvas.height) pen.inside = true;
  else pen.inside = false;

  if(pen.select == 'text'){
      //texting
      if(text.texting == false){
          if(pen.inside){
            text.texting = true;
            textBox.style['z-index'] = 5;
            textBox.style.left = pos.x - 2;
            textBox.style.top = pos.y - 2;
            textBox.style.color = ctx.strokeStyle;
          }
      }
      //end of texting
      else{
          if(e.clientX>=parseInt(textBox.style.left) && e.clientX<=parseInt(textBox.style.left)+5*parseInt(text.size) && e.clientY>=parseInt(textBox.style.top) && e.clientY<=parseInt(textBox.style.top)+parseInt(text.size)) return;
          var textContent = textBox.value;
          text.texting = false;
          textBox.style['z-index'] = 1;
          textBox.value = "";
          if(!textContent) return;
          else{
            ctx.fillText(textContent, canvaspos(parseInt(textBox.style.left)), canvaspos(parseInt(textBox.style.top))+(text.size/2));
          }
      }
  }
  else if(pen.select == 'line' || pen.select == 'circle' || pen.select == 'triangle' || pen.select == 'rect'){
      //change layer to hcanvas
      hcanvas.style['zIndex'] = 7;
      //clear hcanvas
      hctx.clearRect(0, 0, hcanvas.width, hcanvas.height);
  }
}

//return real mouse position in canvas
function canvaspos(pos){
    return pos - parseInt(canvas.style.left);
};

//return Eucild distance
function dist(x1, x2, y1, y2){
    var a = x2 - x1;
    var b = y2 - y1;
    return Math.sqrt(a*a + b*b);
}

//clean the canvas
function clean(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    historys.Push();
}

//undo
function undo(){
    historys.undo();
}

//redo
function redo(){
    historys.redo();
}

//resizeCanvas
function resizeCanvas(){
    canvas.width = hcanvas.width = 800;
    canvas.height = hcanvas.height = 600;
    //hide resize btn
    var resiezbtn = document.getElementById('resizebtn');
    resiezbtn.style.visibility = "hidden"; 
}

//mouseup
function save_hisState(e){
    //hide hcanvas
    hcanvas.style["z-index"] = 3;
    //copy hcanvas to canvas
    if(pen.select == 'line'){
        ctx.beginPath();
        ctx.moveTo(canvaspos(pos.x)+12, canvaspos(pos.y)+12);
        ctx.lineTo(canvaspos(e.clientX)+12, canvaspos(e.clientY)+12);
        ctx.lineWidth = pen.width;
        ctx.lineJoin = ctx.lineCap = 'round';
        ctx.stroke();
    }
    else if(pen.select == 'circle'){
        ctx.beginPath();
        ctx.lineWidth = pen.width;
        ctx.arc(canvaspos(pos.x)+12, canvaspos(pos.y)+12, dist(pos.x, e.clientX, pos.y, e.clientY), 0, 2*Math.PI);
        ctx.stroke();
    }
    else if(pen.select == 'rect'){
        //draw rect
        ctx.beginPath();
        ctx.lineWidth = pen.width;
        ctx.rect(canvaspos(pos.x)+12, canvaspos(pos.y)+12, (e.clientX-pos.x), (e.clientY-pos.y));
        ctx.stroke();
    }
    else if(pen.select == 'triangle'){
        //draw circle
        ctx.beginPath();
        ctx.lineWidth = pen.width;
        //three notchs
        var a = {x: canvaspos(pos.x)+12, y: canvaspos(pos.y)+12};
        var b = {x: canvaspos(e.clientX)+12, y: canvaspos(pos.y)+12};
        var c = {x: canvaspos((pos.x+e.clientX)/2)+12, y: canvaspos(e.clientY)+12};
        ctx.moveTo(a.x, a.y);
        ctx.lineTo(b.x, b.y);
        ctx.lineTo(c.x, c.y);
        ctx.lineTo(a.x, a.y);
        ctx.closePath();
        ctx.stroke();
    }

    if(e.clientX>=120 && e.clientX<=120+canvas.width && e.clientY>=120 && e.clientY<=120+canvas.height){
        historys.Push();
    }
    
}

function draw(e) {
    if (e.buttons !== 1) return; // if mouse is not clicked, do not go further
    
    //line properties
    //ctx.color = pen.color;
    ctx.lineWidth = pen.width;

    if(pen.select == 'pen'){
        ctx.beginPath();
        //dot first
        ctx.arc(canvaspos(pos.x), canvaspos(pos.y)+24, ctx.lineWidth/2, 0, Math.PI*2);
        ctx.fill();
        ctx.beginPath();
        //draw line
        ctx.moveTo(canvaspos(pos.x), canvaspos(pos.y)+24); // from position
        setPosition(e);
        ctx.lineTo(canvaspos(pos.x), canvaspos(pos.y)+24); // to position
        ctx.stroke(); // draw it!
    }
    else if(pen.select == 'eraser'){
        ctx.globalCompositeOperation="destination-out";
        ctx.beginPath();
        //dot first
        ctx.arc(canvaspos(pos.x), canvaspos(pos.y)+24, ctx.lineWidth/2, 0, Math.PI*2, false);
        ctx.fill();
        ctx.beginPath();
        //draw line
        ctx.moveTo(canvaspos(pos.x), canvaspos(pos.y)+24); // from position
        setPosition(e);
        ctx.lineTo(canvaspos(pos.x), canvaspos(pos.y)+24); // to position
        ctx.stroke(); // draw it!
        //recover state
        ctx.globalCompositeOperation="source-over";
    }
    else if(pen.select == 'line'){
        //clear hcanvas
        hctx.clearRect(0, 0, hcanvas.width, hcanvas.height);
        //draw line
        hctx.beginPath();
        hctx.moveTo(canvaspos(pos.x)+12, canvaspos(pos.y)+12);
        hctx.lineTo(canvaspos(e.clientX)+12, canvaspos(e.clientY)+12);
        hctx.lineWidth = pen.width;
        hctx.lineJoin = hctx.lineCap = 'round';
        hctx.stroke();
    }
    else if(pen.select == 'circle'){
        //clear hcanvas
        hctx.clearRect(0, 0, hcanvas.width, hcanvas.height);
        //draw circle
        hctx.beginPath();
        hctx.lineWidth = pen.width;
        hctx.arc(canvaspos(pos.x)+12, canvaspos(pos.y)+12, dist(pos.x, e.clientX, pos.y, e.clientY), 0, 2*Math.PI);
        hctx.stroke();
    }
    else if(pen.select == 'triangle'){
        //clear hcanvas
        hctx.clearRect(0, 0, hcanvas.width, hcanvas.height);
        //draw circle
        hctx.beginPath();
        hctx.lineWidth = pen.width;
        //three notchs
        var a = {x: canvaspos(pos.x)+12, y: canvaspos(pos.y)+12};
        var b = {x: canvaspos(e.clientX)+12, y: canvaspos(pos.y)+12};
        var c = {x: canvaspos((pos.x+e.clientX)/2)+12, y: canvaspos(e.clientY)+12};
        hctx.moveTo(a.x, a.y);
        hctx.lineTo(b.x, b.y);
        hctx.lineTo(c.x, c.y);
        hctx.lineTo(a.x, a.y);
        hctx.closePath();
        hctx.stroke();
    }
    else if(pen.select == 'rect'){
        //clear hcanvas
        hctx.clearRect(0, 0, hcanvas.width, hcanvas.height);
        //draw rect
        hctx.beginPath();
        hctx.lineWidth = pen.width;
        hctx.rect(canvaspos(pos.x)+12, canvaspos(pos.y)+12, (e.clientX-pos.x), (e.clientY-pos.y));
        hctx.stroke();
    }
}

//upload image
function uploadimg(){
    var imgInput = document.createElement('input');
	imgInput.type = 'file';
    imgInput.click();
    //when imgInput changed
    imgInput.addEventListener('change', function(e) {
        if(e.target.files) {
          let imageFile = e.target.files[0];
          var reader = new FileReader();
          //convert file to DataURL
          reader.readAsDataURL(imageFile);
          //once file is fully onloaded, draw it (by new image object)
          reader.onloadend = function (e) {
            var myImage = new Image();
            myImage.src = e.target.result;
            myImage.onload = function(ev) {
                if(myImage.width > canvas.width || myImage.height > canvas.height){
                    alert('upload image size is too big !!! (limit:800 x 600px)');
                }
                else{
                    var result = confirm('canvas resize to image size (' + myImage.width + ' x ' + myImage.height + 'px)?');
                    if(result){
                        canvas.width = hcanvas.width = myImage.width;
                        canvas.height = hcanvas.height = myImage.height;
                        //show resize btn
                        var resizebtn = document.getElementById('resizebtn');
                        resizebtn.style.visibility = "visible";
                    }
                    ctx.drawImage(myImage,0,0);
                }
            }
          }
        }
    });
}

//save image
function downloadimg(){
    var imgSrc = canvas.toDataURL();
    var aLink = document.createElement('a');
	aLink.href = imgSrc;
    aLink.download = "pic.png";
    aLink.click();
}